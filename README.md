# Word Paths

## Problem definition
This program finds "word paths."  What this means is that you find a path from one word to another word, changing one letter each step, and each intermediate word must be in the dictionary.
 
Some example solutions:
 
    flux -> flex -> flem -> alem
    rial -> real -> feal -> foal -> foul -> foud
    dung -> dunt -> dent -> gent -> geet -> geez
    doeg -> dong -> song -> sing -> sink -> sick
    jehu -> jesu -> jest -> gest -> gent -> gena -> guna -> guha
    broo -> brod -> brad -> arad -> adad -> adai -> admi -> ammi -> immi
    yagi -> yali -> pali -> palp -> paup -> plup -> blup
    bitt -> butt -> burt -> bert -> berm -> germ -> geum -> meum
    jina -> pina -> pint -> pent -> peat -> prat -> pray
    fike -> fire -> fare -> care -> carp -> camp
 
The program takes as input the path to the word file, a start word, and an end word, and prints out one or more paths from start to end, or indicates that there is no possible path.

## Installation

1. Download and install NodeJS v10 from http://nodejs.org.
2. Install with dependencies

    `npm i -g`

This will install dependencies, and install this module as the executable "wordpath". 

## Usage

    usage:  wordpath <startWord> <endWord> [-1] [-q]
                 -1   return first path found. Defaults to finding shortest path
                 -q   quiet; do not display progress while running
                 Find a word path from startWord to endWord.

            wordpath <word>
                 Display all the words in the dictionary with the
                 same length as the given word, sorted by simularity
                 to given word.

## Development

1. install as link package for easy debugging. This also installs dependencies

    `npm link`

2. Run unit tests

    `npm test`

3. View coverage report

    `open coverage/lcov-report/index.html`





