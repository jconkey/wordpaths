var WordPath = require('./word-path'),
    Dictionary = require('./dictionary');

var WordPathFinder = function (verbose, onlyFirstResult) {
    this.dict = new Dictionary();
    this.verbose = verbose;
    this.onlyFirstResult = onlyFirstResult;
};

WordPathFinder.prototype = {
    init: function (callback) {
        this.dict.load(callback);
    },

    /**
     * Recursively find a path from the current wordPath to the end word
     * if possible. If a path is found, an array of the words is returned.
     * If not, false is returned.
     *
     * Considers all combinations, except that discards paths longer than
     * paths already found.
     */
    findHelper: function (wordPath) {
        if (this.verbose) {
            process.stdout.write('  ' + wordPath.getWords().join(' -> ') + "\r");
        }
        for (var i=0; i<this.candidateWords.length; i++) {
            var word = this.candidateWords[i];

            if (wordPath.canAppend(word)) {
                var newWordPath = wordPath.clone(),
                    swpl,
                    nwpl,
                    results;

                newWordPath.append(word);

                swpl = this.shortestWordPath.length;
                nwpl = newWordPath.getWords().length;

                if (word === this.endWord) {
                    // end case
                    if (swpl === 0 || nwpl < swpl) {
                        this.shortestWordPath = newWordPath.getWords();
                    }
                    if (this.verbose) {
                        console.log('  ' + newWordPath.getWords().join(' -> ') + ' (' + newWordPath.getWords().length + ')');
                    }
                    if (this.onlyFirstResult) {
                        return true;
                    }
                } else {
                    // recursive case
                    if (swpl === 0 || nwpl < swpl) {
                        if (this.findHelper(newWordPath)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    },

    find: function (startWord, endWord) {
        var wordPath;

        startWord = startWord.toLowerCase();
        endWord = endWord.toLowerCase();

        this.shortestWordPath = [];

        if (this.dict.isDefined(startWord) && this.dict.isDefined(endWord)) {
            this.endWord = endWord;
            this.candidateWords = this.getCandidateWords(endWord);
            wordPath = new WordPath();
            wordPath.append(startWord);
            this.findHelper(wordPath);
        }

        return this.shortestWordPath;
    },

    getCandidateWords: function (endWord) {
        var candidateWords = this.dict.wordsOfLength(endWord.length);
        candidateWords.sort(function(word1, word2) {
            diff1 = WordPath.countDifferences(word1, endWord);
            diff2 = WordPath.countDifferences(word2, endWord);
            if (diff1 < diff2) {
                return -1;
            } else if (diff1 > diff2) {
                return 1;
            }
            return 0;
        });
        return candidateWords;
    }
};

module.exports = WordPathFinder;
