#!/usr/bin/env node

var WordPathFinder = require('./word-path-finder'),
    args = process.argv.slice(2),
    word1,
    word2,
    wpf,
    quiet = false,
    onlyFirstResult = false;

if (args.length === 0) {
	console.log('usage:  wordpath <startWord> <endWord> [-1] [-q]');
	console.log('             -1   return first path found. Defaults to finding shortest path');
	console.log('             -q   quiet; do not display progress while running');
	console.log('             Find a word path from startWord to endWord.');
	console.log('');
	console.log('        wordpath <word>');
	console.log('             Display all the words in the dictionary with the');
	console.log('             same length as the given word, sorted by simularity');
	console.log('             to given word.');
	process.exit();
}

word1 = args[0];
word2 = args[1];

for (var i=2; i<4; i++) {
	if (args[i] === '-q') {
		quiet = true;
	}
	if (args[i] === '-1') {
		onlyFirstResult = true;
	}
}

wpf = new WordPathFinder(!quiet, onlyFirstResult);
wpf.init(function (err) {
    if (err) {
    	console.log(err);
    	process.exit();
    }

    if (!word2) {
    	console.log("Candidate words for " + word1 + ":\n", 
    		wpf.getCandidateWords(word1).join(', '));
    } else {
	    var wordPath = wpf.find(word1, word2);
		if (0 === wordPath.length) {
	        console.log('There is no word path from "' 
	        	+ word1 
	        	+ '" to "'
	            + word2 + '".');
	    } else {
	    	console.log('Found: ' + wordPath.join(' -> ') + ' (' + wordPath.length + ')');
	    }
	}
});
