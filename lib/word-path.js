var WordPath = function () {
    this.words = [];
    this.hasMap = {};
};

WordPath.countDifferences = function (word1, word2) {
	var diffCount = 0;
    for (var i=0; i<word1.length; i++) {
        diffCount += word1[i] !== word2[i];
    }        
    return diffCount;
};

WordPath.prototype = {
    getWords: function () {
        return this.words;
    },

    append: function (word) {
        this.words.push(word);
        this.hasMap[word] = true;
    },

    clone: function () {
        var newWordPath = new WordPath();
        for (var i=0; i<this.words.length; i++) {
            newWordPath.append(this.words[i]);
        }
        return newWordPath;
    },

    hasWord: function (word) {
        return !!this.hasMap[word];
    },

    // assumes given word is same length as other 
    // words already in the list
    canAppend: function (word) {
        var numWords = this.words.length;

        if (numWords === 0) {
            return true;
        }

        if (this.hasWord(word)) {
        	return false;
        }

        return 1 === WordPath.countDifferences(word, this.words[numWords - 1]);
    }
};    

module.exports = WordPath;
