var fs = require('fs'),
    DICTIONARY_FILE = '/usr/share/dict/words';

var Dictionary = function () {
    this.map = {};
};

Dictionary.prototype = {
    loadDictionaryFile: function (callback) {
        fs.readFile(DICTIONARY_FILE, {encoding: 'utf8'}, callback);
    },

    load: function (callback) {
        var that = this;
        this.loadDictionaryFile(function (err, data) {
            var words;

            if (err) {
                return callback(err);
            }

            words = data.toLowerCase().split(/\s+/);
            for (var i=0; i<words.length; i++) {
                var word = words[i], 
                    len = word.length;
                if (!that.map[len]) {
                    that.map[len] = {};
                }
                that.map[len][word] = true;
            }

            callback(null, that.map);
        });
    },

    isDefined: function (word) {
        var len = word.length;
        return !!this.map[len] && !!this.map[len][word];
    },

    wordsOfLength: function (len) {
        return Object.keys(this.map[len]);
    }
};

module.exports = Dictionary;
