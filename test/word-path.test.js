var Y = require('yuitest'),
    Assert = Y.Assert,
    WordPath = require('../lib/word-path');

Y.TestRunner.add(new Y.TestCase({

    name: 'Wordpath Unit Tests',

    setUp: function () {
    },

    tearDown: function () {
    },

    "Should append words and return current word list": function () {
        var wp = new WordPath(),
            words;
        wp.append('flux');
        wp.append('flex');
        words = wp.getWords();
        Assert.areEqual(2, words.length);
        Assert.areEqual('flux', words[0]);
        Assert.areEqual('flex', words[1]);
    },

    "Should be able to clone itself": function () {
        var wp = new WordPath(),
            clonedWp,
            words;
        wp.append('flux');
        wp.append('flex');
        clonedWp = wp.clone();
        clonedWp.append('flem');

        words = wp.getWords();
        Assert.areEqual(2, words.length);
        Assert.areEqual('flux', words[0]);
        Assert.areEqual('flex', words[1]);

        words = clonedWp.getWords();
        Assert.areEqual(3, words.length);
        Assert.areEqual('flux', words[0]);
        Assert.areEqual('flex', words[1]);
        Assert.areEqual('flem', words[2]);
    },

    "Should detect if a word is in the list already": function () {
        var wp = new WordPath();
        Assert.areEqual(false, wp.hasWord('flux'));
        wp.append('flux');
        Assert.areEqual(true, wp.hasWord('flux'));
    },

    "Should detect if can append a new word to the list": function () {
        var wp = new WordPath(),
            words;
        Assert.areEqual(true, wp.canAppend('yes'), 'cannot append the first word');
        wp.append('yes');
        Assert.areEqual(false, wp.canAppend('no'), 'allowed an invalid word');
        Assert.areEqual(false, wp.canAppend('xyz'), 'allowed an invalid word');
        Assert.areEqual(true, wp.canAppend('yep'), 'cannot append a valid word');
    },

    "Should count letter differences between two words": function () {
        Assert.areEqual(0, WordPath.countDifferences('flux', 'flux'), 'got wrong difference count');
        Assert.areEqual(1, WordPath.countDifferences('flux', 'flex'), 'got wrong difference count');
        Assert.areEqual(3, WordPath.countDifferences('abc', 'xyz'), 'got wrong difference count');
    }
}));
