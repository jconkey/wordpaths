var Y = require('yuitest'),
    mockery = require('mockery'),
    sinon = require('sinon'),
    Assert = Y.Assert,
    fsMock,
    Dictionary,
    wordsFixture,
    wordsFixtureText;

wordsFixture = [
    'A', 'flux', 'flex', 'flem', 'alem', 'rial',
    'real', 'feal', 'foal', 'foul', 'foud',
    'to', 'by', 'me', 'we', 'my'
];
wordsFixtureText = wordsFixture.join("\n");
fsMock = {
    readFile: sinon.stub()
};
fsMock.readFile.yieldsAsync(null, wordsFixtureText);

Y.TestRunner.add(new Y.TestCase({

    name: 'Dictionary Unit Tests',

    setUp: function () {
        mockery.enable({warnOnUnregistered: false});
        mockery.registerMock('fs', fsMock);
        Dictionary = require('../lib/dictionary');
    },

    tearDown: function () {
        mockery.deregisterAll();
        mockery.disable();
    },

    "Should load dictionary file": function () {
        var d = new Dictionary();
        d.loadDictionaryFile(function (err, text) {
            this.resume(function () {
                Assert.isNull(err, 'expected no error');
                Assert.areEqual(wordsFixtureText, text, 'did not load dictionary text');
            });
        }.bind(this));
        this.wait(100);
    },

    "Should load dictionary as lowercase words": function () {
        var d = new Dictionary();
        d.load(function (err, map) {
            this.resume(function () {
                Assert.isNull(err, 'expected no error');
                wordsFixture.forEach(function (word) {
                    word = word.toLowerCase();
                    Assert.areEqual(true, map[word.length][word], 'did not load and map word ' + word);
                });
            });
        }.bind(this));
        this.wait(100);
    },

    "Should map loaded words by word length": function () {
        var d = new Dictionary();
        d.load(function (err, map) {
            this.resume(function () {
                Assert.isNull(err, 'expected no error');
                Assert.areEqual(1, Object.keys(d.wordsOfLength(1)).length, 'did not load exactly one 1-char word');
                Assert.areEqual(10, Object.keys(d.wordsOfLength(4)).length, 'did not load exactly ten 4-char words');
                Assert.areEqual(5, Object.keys(d.wordsOfLength(2)).length, 'did not load exactly five 2-char words');
            });
        }.bind(this));
        this.wait(100);
    },

    "Should return true if given word is defined otherwise false": function () {
        var d = new Dictionary();
        d.load(function (err, map) {
            this.resume(function () {
                Assert.isNull(err, 'expected no error');
                Assert.areEqual(true, d.isDefined('a'), 'did not detect defined capitalized word');
                Assert.areEqual(true, d.isDefined('foul'), 'did not detect defined lowercase word');
                Assert.areEqual(false, d.isDefined('bird'), 'did not detect undefined word');
            });
        }.bind(this));
        this.wait(100);
    }
}));
