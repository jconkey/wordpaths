var Y = require('yuitest'),
    mockery = require('mockery'),
    sinon = require('sinon'),
    Assert = Y.Assert,
    ArrayAssert = Y.ArrayAssert,
    WordPathFinder,
    wordsFixture,
    wordsFixtureText;

wordsFixture = [
    'A', 'flux', 'alem', 'flex', 'flem', 'rial',
    'foud', 'real', 'foal', 'feal', 'foul',
    'to', 'by', 'me', 'we', 'my'
];
wordsFixtureText = wordsFixture.join("\n");
fsMock = {
    readFile: sinon.stub()
};
fsMock.readFile.yieldsAsync(null, wordsFixtureText);


Y.TestRunner.add(new Y.TestCase({

    name: 'WordPathFinder Unit Tests',

    setUp: function () {
        mockery.enable({warnOnUnregistered: false});
        mockery.registerMock('fs', fsMock);
        WordPathFinder = require('../lib/word-path-finder');
    },

    tearDown: function () {
        mockery.deregisterAll();
        mockery.disable();
    },

    "Should return empty list if no path is possible": function () {
        var wpf = new WordPathFinder();
        wpf.init(function (err) {
            this.resume(function () {
                Assert.isNull(err, 'expected no error');
                Assert.areEqual(0, wpf.find('flux', 'dinosaur').length);
                Assert.areEqual(0, wpf.find('xbxb', 'dinosaur').length);
                Assert.areEqual(0, wpf.find('dinosaur', 'xbxb').length);
                Assert.areEqual(0, wpf.find('abbreviately', 'abbreviation').length);
            });
        }.bind(this));
        this.wait(100);
    },

    "Should return word path if input words have a word path": function () {
        var wpf = new WordPathFinder();
        wpf.init(function (err) {
            this.resume(function () {
                ArrayAssert.itemsAreEqual(
                    ['flux', 'flex'],
                    wpf.find('flux', 'flex'),
                    'did not get word path with words that have a word path');
                ArrayAssert.itemsAreEqual(
                    ['real', 'feal', 'foal', 'foul', 'foud'],
                    wpf.find('real', 'foud'),
                    'did not get word path with words that have a word path');
                ArrayAssert.itemsAreEqual(
                    ['by', 'my', 'me', 'we'],
                    wpf.find('by', 'we'),
                    'did not get word path with words that have a word path');
            });
        }.bind(this));
        this.wait(100);
    }
}));
